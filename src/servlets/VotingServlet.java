package servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class VotingServlet extends HttpServlet {


    @Override
    public void doGet(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException, IOException {


        request.setAttribute("data", request.getRequestURL().toString());
        request.setAttribute("voteId", request.getParameter("voteId"));
        request.getRequestDispatcher("resources/jsp/result.jsp").forward(request, response);
        return;
        /*
        String key = request.getParameter("voteId");
        int data = Integer.valueOf(request.getParameter("data"));

        response.setContentType("text/html");

        request.setAttribute("name", request.getParameter("voteId"));
        request.getRequestDispatcher("resources/jsp/voting.jsp").forward(request, response);
        */
    }

}
