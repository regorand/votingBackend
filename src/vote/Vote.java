package vote;

/**
 * Created by root on 22.11.16.
 */
public class Vote {

    private int scoreCount = 0;
    private int votes = 0;

    public void addVote(int score){
        //if block should always be true
        if(!(score < 0 || score > 10)){
            this.scoreCount += score;
            votes++;
        }
    }

    public int getScore(){
        return scoreCount / votes;
    }

}
