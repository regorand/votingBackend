<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:c="http://java.sun.com/jsp/jstl/core">
<head>
    <link rel="stylesheet" href="resources/css/bootstrap.css">
    <script type="text/javascript" src="resources/js/script.js"></script>
</head>

<body>
<c:if test="${not empty foundNoVote}">
    <p>Error: Vote Id ${voteId} could not be found</p>
    <form action="createvote" method="get">
        <input type="hidden" value="${voteId}" name="voteId">
        <label for="create">create Vote with voteId: ${voteId}?</label>
        <input type="submit" id="create" value="Create">
    </form>
</c:if>

<form method="get" action="vote">
    <input type="text" name="voteId">
    <input type="submit" value="Vote!">
</form>

</body>
</html>
