<html>
<head>

    <link rel="stylesheet" href="resources/css/bootstrap.css">
    <link rel="stylesheet" href="resources/css/voting.css">

    <script type="text/javascript" src="resources/js/script.js"></script>
</head>

<body>

    <h1>${name} = </h1>

    <form id="inputForm" method="POST" action="voted">
        <input type="hidden" name="voteId" value="${name}">
        <input type="hidden" name="data" id="input" value="">
    </form>
    <div class="col-xs-4 onClickDiv" onclick="divClicked(10)">
        <span class="value_span">5/7</span>
    </div>
    <div class="col-xs-4 onClickDiv" onclick="divClicked(9)">
        <span class="value_span">9/10</span>
    </div>
    <div class="col-xs-4 onClickDiv" onclick="divClicked(8)">
        <span class="value_span">8/10</span>
    </div>
    <div class="col-xs-4 onClickDiv" onclick="divClicked(7)">
        <span class="value_span">7/10</span>
    </div>
    <div class="col-xs-4 onClickDiv" onclick="divClicked(6)">
        <span class="value_span">6/10</span>
    </div>
    <div class="col-xs-4 onClickDiv" onclick="divClicked(5)">
        <span class="value_span">5/10</span>
    </div>
    <div class="col-xs-4 onClickDiv" onclick="divClicked(4)">
        <span class="value_span">4/10</span>
    </div>
    <div class="col-xs-4 onClickDiv" onclick="divClicked(3)">
        <span class="value_span">3/10</span>
    </div>
    <div class="col-xs-4 onClickDiv" onclick="divClicked(2)">
        <span class="value_span">2/10</span>
    </div>
    <div class="col-xs-4 onClickDiv" onclick="divClicked(1)">
        <span class="value_span">1/10</span>
    </div>
    <div class="col-xs-4 onClickDiv" onclick="divClicked(0)">
        <span class="value_span">0/10</span>
    </div>

</body>
</html>