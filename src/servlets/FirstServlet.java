package servlets;

import vote.VotingController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class FirstServlet extends HttpServlet{

    @Override
    public void doGet(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException, IOException{

        if(request.getRequestURL().toString().contains("vote")){
               handleVote(request, response);
        } else {
            handleRedirect(request, response);
        }
    }

    private void handleVote(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        response.setContentType("text/html");

        String voteId = request.getParameter("voteId");

        VotingController controller = VotingController.getVotingController();

        if(controller.voteWithIdExists(voteId)){
            request.setAttribute("name", voteId);
            request.getRequestDispatcher("resources/jsp/voting.jsp").forward(request, response);
        } else {
            response.setHeader("error", "true");
            response.sendRedirect("");
        }
    }

    private void handleRedirect(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        response.setContentType("text/html");
        if(request.getHeader("error") == "true"){
            request.setAttribute("foundNoVote", "true");
        }
        request.getRequestDispatcher("index.jsp").forward(request, response);

    }
}
