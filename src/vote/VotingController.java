package vote;

import java.util.HashMap;

/**
 * Created by root on 22.11.16.
 */
public class VotingController {

    private static VotingController votingController = null;

    public static VotingController getVotingController(){
        if(votingController == null){
            votingController = new VotingController();
        }
        return votingController;
    }

    private HashMap<String, Vote> votes;

    public VotingController(){
        votes = new HashMap<>();
    }

    public void createNewVote(String voteId){
        votes.put(voteId, new Vote());
    }

    public void addVote(String voteId, int score){
        votes.get(voteId).addVote(score);
    }

    public int getResultofVote(String voteId){
        return votes.get(voteId).getScore();
    }

    public boolean voteWithIdExists(String voteId){
        return votes.containsKey(voteId);
    }

}
